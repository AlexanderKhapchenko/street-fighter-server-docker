const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  search(search) {
    const fighter = FighterRepository.getOne(search);
    if (!fighter) {
      return null;
    }
    return fighter;
  }

  getAll() {
    const fighters = FighterRepository.getAll();
    if (!fighters) {
      return null;
    }
    return fighters;
  }

  create(fighter) {
    const newFighter = FighterRepository.create(fighter);
    if (!newFighter) {
      return null;
    }
    return newFighter;
  }

  update(id, data) {
    const updatedFighter = FighterRepository.update(id, data);
    if (!updatedFighter) {
      return null;
    }
    return updatedFighter;
  }

  delete(id) {
    const removedFighter = FighterRepository.delete(id);
    if (!removedFighter) {
      return null;
    }
    return removedFighter;
  }
}

module.exports = new FighterService();
