const { FightRepository } = require("../repositories/fightRepository");

class FightersService {
  search(search) {
    const fight = FightRepository.getOne(search);
    if (!fight) {
      return null;
    }
    return fight;
  }

  getAll() {
    const fights = FightRepository.getAll();
    if (!fights) {
      return null;
    }
    return fights;
  }

  create(fight) {
    const newFight = FightRepository.create(fight);
    if (!newFight) {
      return null;
    }
    return newFight;
  }

  update(id, data) {
    const updatedFight = FightRepository.update(id, data);
    if (!updatedFight) {
      return null;
    }
    return updatedFight;
  }

  delete(id) {
    const removedFight = FightRepository.delete(id);
    if (!removedFight) {
      return null;
    }
    return removedFight;
  }
}

module.exports = new FightersService();
