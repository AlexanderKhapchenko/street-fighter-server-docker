const { BaseRepository } = require('./baseRepository');

class FighterRepository extends BaseRepository {
    constructor() {
        super('fighters');
    }

		create(fighter) {
			if (!fighter.health) {
				fighter.health = 100;
			}
			return super.create(fighter);
		}
}

exports.FighterRepository = new FighterRepository();