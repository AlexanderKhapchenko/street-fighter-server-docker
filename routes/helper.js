const { Router } = require('express');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    res.data = 'use api/users or api/fighter or api/fighters';
    next();
}, responseMiddleware);

module.exports = router;