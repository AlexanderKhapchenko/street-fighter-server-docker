const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { errorHandler } = require('../middlewares/error.handler.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.post('/', createUserValid, (req, res, next) => {
	try {
		const {body, body: {email, phoneNumber}} = req;

		if (UserService.search({email})) { 
			throw new Error(`Email ${email} already in use`);
		}

		if (UserService.search({phoneNumber})) {
				throw new Error(`Phone number ${phoneNumber} already in use`);
		}

		res.data = UserService.create(body);
		next();
	} catch ({ message }) {
		next({message, status: 400})
	}
}, responseMiddleware, errorHandler);

router.get('/:id', (req, res, next) => {
	try {
			const { params: {id} }  = req;
			const user = UserService.search({id});

			if(!user) {
					throw new Error('User not found');
			}

			res.data = user;  
			next();  
	} catch ({ message }) {
		next({message, status: 404})
	}
}, responseMiddleware, errorHandler);

router.get('/', (req, res, next) => {
	res.data = UserService.getAll();
	next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
	const { params: { id }}  = req;
	res.data = UserService.delete(id);
	next();
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
	try {
		const { body, params: { id, email, phoneNumber }} = req;

		if (!UserService.search({id})) { 
			throw new Error('User not found');
		}

		if (UserService.search({email})) { 
			throw new Error(`Email ${email} already in use`);
		}

		if (UserService.search({phoneNumber})) {
				throw new Error(`Phone number ${phoneNumber} already in use`);
		}

		res.data = UserService.update(id, body);
		next();
	} catch ({ message }) {
		next({message, status: 400});
	}
}, responseMiddleware, errorHandler);

module.exports = router;