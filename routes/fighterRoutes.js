const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { errorHandler } = require('../middlewares/error.handler.middleware');

const router = Router();

router.post('/', createFighterValid, (req, res, next) => {
	try {
		const {body, body: { name }} = req;

		if (FighterService.search({name})) {
			throw new Error(`This name ${name} is already in use`);
		}

		res.data = FighterService.create(body);
		next();
	} catch ({ message }) {
		next({message, status: 400})
	}
}, responseMiddleware, errorHandler);

router.get('/:id', (req, res, next) => {
	try {
			const { params: { id }}  = req;
			const fighter = FighterService.search({id});

			if(!fighter) {
					throw new Error('Fighter not found');
			}

			res.data = fighter;  
			next();  
	} catch ({ message }) {
		next({message, status: 404})
	}
}, responseMiddleware, errorHandler);

router.get('/', (req, res, next) => {
	res.data = FighterService.getAll();
	next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
	const { params: { id }}  = req;
	res.data = FighterService.delete(id);
	next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
	try {
		const { body, params: { id }} = req;

		if (!FighterService.search({id})) { 
			throw new Error('Fighter not found');
		}

		res.data = FighterService.update(id, body);
		next();
	} catch ({ message }) {
		next({message, status: 400});
	}
}, responseMiddleware, errorHandler);

module.exports = router;