const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
		try {
			isBodyHaveAllRequired(req);
			validateFields(req);

			next();
		} catch ({message}) {
			next({message, status: 400})
		}
}

const isBodyHaveAllRequired = ({ body }) => {
	const { id, ...required } = user;
	const requiredKeys = JSON.stringify(Object.keys(required).sort());
	const bodyKeys = JSON.stringify(Object.keys(body).sort());

	if(requiredKeys != bodyKeys) {
		throw new Error("Incorrect body fields");
	}
}

const validateFields = ({ body }) => {
	validationFullName(body);
	validationPhone(body);
	validaionGmail(body);
	validationPassword(body);
}

const validationFullName = ({ lastName, firstName }) => {
	if (lastName && lastName.replace(/\s/g, '').length < 1) {
		throw new Error("The last name cannot be empty or less than one letter");
	}

	if (firstName && firstName.replace(/\s/g, '').length < 1) {
		throw new Error("The first name cannot be empty or less than one letter");
	}
}

const validationPhone = ({ phoneNumber }) => {
	const re = /^\+380\d{9}$/;
	if (phoneNumber && !re.test(phoneNumber)) {
		throw new Error("Incorrect number. Number must have the format +380XXXXXXXXX");
	}
}

const validaionGmail = ({ email }) => {
	const re = /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/;
	if (email && !re.test(email)) {
		throw new Error("Incorrect email. Email must be from gmail");
	}
}

const validationPassword = ({ password }) => {
	if (password && password.length < 3) {
		throw new Error("Incorrect password. Password must be longer than 3 characters");
	}
}

const updateUserValid = (req, res, next) => {
		try {
			isBodyHaveOneRequired(req);
			validateFields(req);

			next();
		} catch ({ message }) {
			next({message, status: 400})
		}
}

const isBodyHaveOneRequired = ( { body }) => {
	const {id, ...possible} = user;

	if (!Object.keys(body).every(key => possible.hasOwnProperty(key))) {
		throw new Error('Incorrect body fields');
	}

	if(Object.keys(body).length == 0) {
		throw new Error('Body can`t be empty')
	}
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;