FROM node:12-alpine

COPY config ./config
COPY middlewares ./middlewares
COPY models ./models
COPY repositories ./repositories
COPY routes ./routes
COPY services ./services
COPY config ./config
COPY index.js ./
COPY *.json ./

RUN npm install

CMD ["npm", "start"]
EXPOSE 3050